%SCROLLVOLUME let you scroll through volume data
%   fid = SCROLLVOLUME(vol) opens a figure and uses allows scrolling
%   through the given 3D dataset vol. It returns the ID of the used figure.
%
%   fid = SCROLLVOLUME(vol, varargin) allows optional setting/parameter:
%       'clim'          colormap limits that are passed through to imagesc.
%                       Default: [min(vol(:)), max(vol(:))].
%       'defaultSlice'  default slice on startup.
%                       Default: floor(#slices/2).
%       'fid'           If fid is a valid figure handle (open figure) than
%                       SCROLLVOLUME uses the specified figure to draw the
%                       slices. 
%                       Default: create a new figure.
%
%   Example: (run SCROLLVOLUME() without any argument to show example)
%           fid = newFigure;
%   		fid.Name = ['example ',mfilename];
%           fid = scrollVolume(rand(5,5,5), 'fid', fid);
%
%   ToDo: check if properly displayed with rot90(flip(*,3), 3)

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       05-Feb-2018

%   https://gitlab.com/foecke/helperFiles
%%

function fid = scrollVolume(vol, varargin)

    %% run example?
    if nargin==0
        fid = runExample();
        return;
    end
    
    %% parsing    
    p = inputParser;    
    p.addRequired('vol');
    p.addParameter('clim',[],@ismatrix);
    p.addParameter('defaultSlice',max(1,floor(size(vol,3)/2)),@isnumeric);
    p.addParameter('fid', []);
    p.parse(vol, varargin{:});
    
    slice   = p.Results.defaultSlice;
    clim   = p.Results.clim;
    fid     = validateFigure(p.Results.fid);	
    fid.WindowScrollWheelFcn = @scrollWheelFcn;
    
    %% prepare clim
    if isempty(clim)
        clim = [min(vol(:)), max(vol(:))];
    end    
	if clim(1)==clim(2)
		clim(2) = clim(1)+1;
	end
        
    %% function
    
    ccmap = fid.Colormap;
    drawSlice;

    function drawSlice
        
        %check if colorbar is active
        oldCb = findall(fid,'Type','colorbar');
        if ~isempty(oldCb)
            %if so store current colormap
            ccmap = colormap(fid.CurrentAxes);
        end
        
        slice = max(slice, 1);
        slice = min(slice, size(vol,3));        
        
        useFigure(fid);
        subFigure(111,'mt',0.075);
        
        cla(fid.CurrentAxes);
        imagesc(vol(:,:,slice),clim); 
        
        if ~isempty(oldCb)
            %restore colorbar on new plot
            colorbar(fid.CurrentAxes);
            colormap(fid.CurrentAxes,ccmap);
        end
        
		axis image;
		axis off;
        title(['Slice ', num2str(slice)]);
        
    end

    function scrollWheelFcn(~,evnt)
        
        slice = slice + evnt.VerticalScrollCount;
        drawSlice;
        
    end

    %% example
    function fid = runExample()
		
        fid = newFigure;
		fid.Name = ['example ',mfilename];
        fid = scrollVolume(rand(5,5,5), 'fid', fid);
        
    end

end
