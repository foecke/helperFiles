%   ADDGAUSSIANNOISESNRDB adds noise with a given SNR with signals in dB
%   Usage: gSNRdB = addGaussianNoiseSNRdB(g, SNRdB)

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       15-Mar-2018

%   https://gitlab.com/foecke/helperFiles
%%

function gSNRdB = addGaussianNoiseSNRdB(g, SNRdB)
    
    % Signal Root Means Square (RMS)
    signal_rms  = sqrt(mean(g(:).^2));
    
    % SNR_db    = 20 * log_10 ( RMS_signal / RMS_noise )
    % RMS_noise = Noise Standard Deviation
    noise_rms   = signal_rms/(10^(SNRdB/20)); 
    
    noise       = noise_rms*randn(size(g));
    gSNRdB      = g + noise; 
    
end