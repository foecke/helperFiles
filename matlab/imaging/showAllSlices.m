%SHOWALLSLICES show all slices of volume data next to each other
%   fid = showAllSlices(volStack) opens a figure and show all slices of
%   volumes given in a cell array of volumes volStack.
%
%   fid = SHOWALLSLICES(volStack, varargin) allows optional 
%   setting/parameter:
%   	'titles'        cell array of titles. The i-th entry in the title
%                       cell is assigned to the i-th volume in the stack. 
%                       If no i-th entry is defined in the image stack, the
%                       i-th title is ignored.
%       'clim'          colormap limits that are passed through to imagesc.
%                       The following options are available:
%                       'self'  every volume in the stack is scaled
%                               according to its minimum and maximum value.
%                       'all'   all volumes are scaled accourding to the
%                               minimum and maximum values of all images in
%                               the stack.
%                       [x,y]   all images are scaled according to the
%                               range [x,y].
%                       Default: 'all'.
%   	'titleInterpreter'  Interpretation of text characters in titles.
%                       See Title->Interpreter for more information.
%                       Options: 'tex', 'latex' and 'none' (default).
%   	'fid'           If fid is a valid figure handle (open figure) than
%                       SCROLLVOLUME uses the specified figure to draw the
%                       slices. 
%                       Default: create a new figure.
%       'update'        set what volumes are drawn by calling SHOWALLSLICES
%                       (only useful by updating an existing figure showing
%                       SHOWALLSLICES)
%                       'all'   all volumes are updated
%                       'last'  only the last volume is updated
%                       p       only the p-th volume is updated
%                       Default: 'all'.
%
%   Example: (run SHOWALLSLICES() without any argument to show example)
%           
%       volStack{1} = rand(5,5,5);
%       volStack{2} = rand(10,10,5);
%       volStack{3} = rand(15,15,10);
% 
%       titleStack{1} = '5x5x5 Random Matrix';
%       titleStack{2} = '10x10x5 Random Matrix';
%       titleStack{3} = '15x15x10 Random Matrix';
%       fid = newFigure;
%       fid.Name = ['example ',mfilename];
%       fid = showAllSlices(volStack, 'fid', fid, 'titles', titleStack);
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       05-Feb-2018

%   https://gitlab.com/foecke/helperFiles
%%

function fid = showAllSlices(volStack, varargin)

    %% run example?
    if nargin == 0
        fid = runExample;
        return;
    end
    
    %% parsing
    p = inputParser;
    
    p.addRequired('volStack');
    p.addParameter('titles',{},@iscell);
    p.addParameter('clim',[],@(s) any([strcmp({'self', 'all'}, s), ismatrix(s)]));
    p.addParameter('fid', gcf);
	p.addParameter('titleInterpreter', 'none', @(s) any(strcmp({'tex', 'latex', 'none'}, s)));
	p.addParameter('update', 'all', @(s) any([strcmp({'all', 'last'}, s), isnumeric(s)]));
       
    p.parse(volStack, varargin{:});
    
    volStack            = p.Results.volStack;
    titleStack          = p.Results.titles;
    clim               = p.Results.clim;
	titleInterpreter    = p.Results.titleInterpreter;
	updateP             = p.Results.update;
    fid                 = validateFigure(p.Results.fid);	
    
	%% parse stacks    
    aux = ~cellfun('isempty',volStack);     
	ind = 1:numel(volStack);
    titleStack(numel(titleStack)+1:numel(volStack)) = {[]};
    titleStack(numel(volStack)+1:numel(titleStack)) = [];    
    volStack = volStack(aux);
    titleStack = titleStack(aux);
	ind = ind(aux);
	
    %% parse clim
	climStack = zeros(numel(volStack), 2);
	
	if ismatrix(clim) && size(clim, 1)==1 && size(clim, 2)==2
		climStack = repmat(clim, [numel(titleStack), 1]);
	elseif ismatrix(clim) && size(clim, 1)==1 && size(clim, 2)==1	&& any(ind==clim)	
		climStack = repmat([min(volStack{ind==clim}(:)), max(volStack{ind==clim}(:))], [numel(titleStack), 1]);
	elseif strcmp(clim, 'self')
		for i = 1:numel(volStack)
			climStack(i, :) = [min(volStack{i}(:)), max(volStack{i}(:))];
		end
	else
		clim = [min(volStack{1}(:)), max(volStack{1}(:))];     
		for j = 2:numel(volStack)
			if clim(1)>min(volStack{j}(:))
				clim(1)=min(volStack{j}(:));
			end
			if clim(2)<max(volStack{j}(:))
				clim(2)=max(volStack{j}(:));
			end
		end
		climStack = repmat(clim, [numel(titleStack), 1]);
	end
			
	aux = (climStack(:,1)==climStack(:,2));
	climStack(aux,2) = climStack(aux,2) + 1;
   
    %% init    
    numVol = numel(volStack);
	numImMax = max(cell2mat(cellfun(@(x) size(x,3),volStack,'uni',false)));
	numTitles = sum(~cellfun(@isempty,titleStack));
        	
	%% calculate subFigure settings	
	s = 2;              %spacing between images in pixel
	ts = 25;            %max spacing for titles in pixel
	
	x = 1000;           %figure width
	xi = x/numImMax;    %size of a single subplot
	
	ts = min(floor(xi/(numVol+1)), ts); %make sure all figure still fit	
	tsRel = ( (ts-s) / ((xi-s) + numTitles*(ts/2)) )/2;

	y = numVol * (xi+2*s) + numTitles * (ts+s); %figure height
	
    
    resizeFigure(fid, [y,x]);
%     %fid.Position(1) = fid.Position(1) - max(0,x-fid.Position(3)); % move figure so everything fits the screen
% 	fid.Position(3) = x;
%     fid.Position(2) = fid.Position(2) - max(0,y-fid.Position(4)); % move figure so everything fits the screen
% 	fid.Position(4) = y;
			
	sRelx = s/x;
	sRely = s/y;
	
	subS.sh	= sRelx;
	subS.sv = sRely;
	subS.mt = 0;
	subS.mb = numTitles * tsRel;
	subS.ml = 0;
	subS.mr = 0;
	subS.pt = 0;
	subS.pb = 0;
	subS.pl = 0;
	subS.pr = 0;
	
    %% check updates
	if  isnumeric(updateP)
		updateI = updateP;
	elseif strcmp(updateP, 'last')
		updateI = numVol;
	else
		updateI = 1:numVol;
	end
    
    %% function	
 	for i = 1:numVol
		
		if ischar(titleStack{i})
			subS.mb = subS.mb - tsRel;
			subS.mt = subS.mt + tsRel;
		end
		if ismember(i, updateI)	
			
			cVol = volStack{i}; %current Volume 
            
            if ~ndims(cVol)==3
                cRes = [size(cVol), 1];
            else
                cRes = size(cVol);
            end

			subFigureSettings = [fieldnames(subS),struct2cell(subS)]';        

			for j = 1:cRes(end)
                if j>numImMax
					continue
                end
                useFigure(fid);
				subFigure(numVol,numImMax,(i-1)*numImMax+j,subFigureSettings{:});
				imagesc(squeeze(cVol(:,:,j)),climStack(i,:));
				axis off;
				if j==1 && ischar(titleStack{i})
					title(titleStack{i}, 'horizontalAlignment', 'left', 'Interpreter', titleInterpreter);
				end
			end
			drawnow;
		end
	end
	
	%% example	
    function fid = runExample()
		
		volStack{1} = rand(5,5,5);
		volStack{2} = rand(10,10,5);
		volStack{3} = rand(15,15,10);

		titleStack{1} = '5x5x5 Random Matrix';
		titleStack{2} = '10x10x5 Random Matrix';
		titleStack{3} = '15x15x10 Random Matrix';
		fid = newFigure;
		fid.Name = ['example ',mfilename];
		fid = showAllSlices(volStack, 'fid', fid, 'titles', titleStack);

    end
end
