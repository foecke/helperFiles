%SHOWMULTIPLEIMAGES shows all images in a cell array
%   fid = SHOWMULTIPLEIMAGES(imageCell) creates an overview of all 2D
%   images that are stored in a cell imageCell. Empty cell entries are
%   ignored. imageCell may also be 2 dimensional to create a 2D grid of
%   images.
%   fid = SHOWMULTIPLEIMAGES(imageCell, varargin) allows optional 
%   setting/parameter:
%   	'titles'        cell array of titles. The i-th entry in the title
%                       cell is assigned to the i-th image in the cell. If
%                       no i-th entry is defined in the image cell, the
%                       i-th title is ignored.
%       'clim'          colormap limits that are passed through to imagesc.
%                       The following options are available:
%                       'self'  every image in the cell is scaled
%                               according to its minimum and maximum value.
%                       'all'   all images are scaled accourding to the
%                               minimum and maximum values of all images in
%                               the cell.
%                       [x,y]   all images are scaled according to the
%                               range [x,y].
%                       {i}     all images are scaled according to the
%                               minimum and maximum values of the i-th
%                               images in the cell.
%                       {[i,j]} or {i,j}    all images are scaled according
%                               to the minimum and maximum values of the
%                               image at cell position (i,j)
%                       Default: 'all'.
%   	'titleInterpreter'  Interpretation of text characters in titles.
%                       See Title->Interpreter for more information.
%                       Options: 'tex', 'latex' and 'none' (default).
%   	'fid'           If fid is a valid figure handle (open figure) than
%                       SCROLLVOLUME uses the specified figure to draw the
%                       slices. 
%                       Default: create a new figure.
%
%   Example: (run SHOWMULTIPLEIMAGES() without any argument to show example)
%       imageCell{1}   = rgb2gray(imread('peppers.png'));
%       titleCell{1}   = 'peppers';
%       imageCell{4}   = rgb2gray(imread('concordaerial.png'));
%       imageCell{27}  = rgb2gray(imread('saturn.png'));
%       titleCell{17}  = 'ignored title';
%       
%       fid         = newFigure;
%       fid.Name    = ['example ',mfilename];            
% 		showMultipleImages(imageCell, 'titles', titleCell, 'fid', fid);
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       22-Mar-2017

%   https://gitlab.com/foecke/helperFiles
%%

function fid = showMultipleImages(imageCell, varargin)

    %% run example?
    if nargin == 0
        fid = runExample;
        return;
    end
    
    %% parsing    
    p = inputParser;
    
    p.addRequired('imageCell');
    p.addParameter('titles',{},@iscell);
    p.addParameter('clim','all',@(s) any([strcmp({'self', 'all'}, s), ismatrix(s), iscell(s)]));
	p.addParameter('titleInterpreter', 'none', @(s) any(strcmp({'tex', 'latex', 'none'}, s)));
    p.addParameter('fid', []);       
       
    p.parse(imageCell, varargin{:});
    
    imageCell           = p.Results.imageCell;
    titleCell           = p.Results.titles;
	titleInterpreter    = p.Results.titleInterpreter;
    clim                = p.Results.clim;
    fid                 = validateFigure(p.Results.fid);	
    
    %% parse cells
    ind = 1:numel(imageCell);
    aux = ~cellfun('isempty',imageCell);
    [ix,iy] = size(imageCell);
    [tx,ty] = size(titleCell);
    titleCell(tx+1:ix,1) = {[]};
    titleCell(ix+1:tx,:) = [];    
    titleCell(1,ty+1:iy) = {[]};
    titleCell(:,iy+1:ty) = [];  
    titleCell(~aux) = {[]};
        
	ind = ind(aux);
    
    %% parse clim
	climCell = cell(size(imageCell));
	
    % catching all cell cases
	if iscell(clim)
        if numel(clim) == 1
            if numel(clim{1}) == 1
                [i,j] = ind2sub(size(imageCell), clim{1});
            elseif numel(clim{1}) == 2
                i = clim{1}(1);
                j = clim{1}(2);
            else 
                warning('clim value not valid'); 
            end
        elseif numel(clim) == 2
            i = clim{1};
            j = clim{2};            
        else
            warning('clim value not valid'); 
        end

        if (i > ix) 
            warning('clim (1st value: %i) out of range -> reset to: %i', i, ix); 
            i = ix; 
        end
        if (j > iy) 
            warning('clim (2nd value: %i) out of range -> reset to: %i', j, iy); 
            j = iy; 
        end        
        climCell(:) = {[min(imageCell{i,j}(:)), max(imageCell{i,j}(:))]};  
       
    % catching simple clim case
    elseif ismatrix(clim) && size(clim, 1)==1 && size(clim, 2)==2
        climCell(:) = {clim};
         
    % catching 'self' case
	elseif strcmp(clim, 'self')
        for i = 1:ix
            for j = 1:iy
                climCell{i, j} = [min(imageCell{i,j}(:)), max(imageCell{i,j}(:))];
            end
        end
        
    %default to 'all'
	else 
		clim = [min(imageCell{ind(1)}(:)), max(imageCell{ind(1)}(:))];    
        for i = ind(2:end)
            if clim(1)>min(imageCell{i}(:))
                clim(1)=min(imageCell{i}(:));
            end  
			if clim(2)<max(imageCell{i}(:))
				clim(2)=max(imageCell{i}(:));
			end          
        end
		climCell(:) = {clim};
	end
    
    % catching unvalid clim
	for i = ind
        if (climCell{i}(1)==climCell{i}(2))
            climCell{i}(2) = climCell{i}(2) + 1;
        end
	end
    
    %% init     
    spacing = 0.025;                            % spacing
    imh = 200;                                  % image height
    maxh = 500;                                 % max height of figure
    p4s             = (imh*spacing)*(ix+1);     % vertival pixel used for spacing
    r               = maxh/(imh*ix + p4s);      % downscaling factor, to match max height
    p4              = min(maxh,imh*ix + p4s);   % calc figure height
    fid.Position(2) = fid.Position(2) - max(0,p4-fid.Position(4)); % move figure so everything fits the screen
    fid.Position(4) = p4;                       % set figure height
    
    imw             = (p4-(p4s*r))/ix;          % image width
    p3s             = (imw*spacing)*(iy+1);     % horizontal pixel used for spacing
    p3              = imw*iy + p3s;             % calc figure width
    %fid.Position(1) = fid.Position(1) - max(0,p3-fid.Position(3)); % move figure so everything fits the screen
    fid.Position(3) = p3;                       % set figure width
        
    
    %% function   
    for i = 1:ix 
        for j = 1:iy
            if ~isempty(imageCell{i,j})
                cis = size(imageCell{i,j}); %current image size
                cir = cis(1)/cis(2); %current image ratio

                s.sv = spacing/ix;
                s.sh = spacing/iy;
                s.mt = 0;
                s.mb = 0;
                s.ml = 0;
                s.mr = 0;
                s.pt = 0;
                s.pb = 0;
                s.pl = 0;
                s.pr = 0;

                
                if cir < 1 % panorama
                    aux = (1-cir)/2;
                    s.pt = aux/iy;
                    s.pb = aux/iy;                  
                elseif cir > 1 % portrait
                    aux = (1-(1/cir))/8;
                    s.pl = aux/ix;
                    s.pr = aux/ix;      
                end        

                if ischar(titleCell{i,j})
                    s.pt = 25/(imw*ix);
                   % s.pb = 5/(imw*ix);
%                     s.pl = s.pl*s.mt;
%                     s.pr = s.pr*s.mt;
                end
                
                subFigureSettings = [fieldnames(s),struct2cell(s)]';  
                useFigure(fid);
                ax = subFigure(ix,iy,j,i,subFigureSettings{:});
                cla(ax);
                imagesc(squeeze(imageCell{i,j}),climCell{i,j});

                if ischar(titleCell{i,j})
                    title(titleCell{i,j}, 'Interpreter', titleInterpreter);
                end

                axis off;
                axis image;
                hold on;
                drawnow;
            end
        end
    end
    
    
    %% example
    function fid = runExample()    
        
        clear imageCell titleCell
        imageCell{1,1}   = rgb2gray(imread('saturn.png'));
        imageCell{1,2}   = rgb2gray(imread('peppers.png'));
        titleCell{1,2}  = 'another title';
        imageCell{1,3}   = rgb2gray(imread('peppers.png'));
        imageCell{1,4}   = rgb2gray(imread('peppers.png'));
        imageCell{2,1}  = rgb2gray(imread('saturn.png'));
        imageCell{2,2}  = rgb2gray(imread('saturn.png'));
        imageCell{2,3}  = rgb2gray(imread('saturn.png'));
        titleCell{2,3}  = 'more title';
        imageCell{1,6}   = rgb2gray(imread('concordaerial.png'));

        imageCell{3,1}   = rand(100);
        imageCell{3,2}   = rand(100);
        imageCell{3,3}   = rand(100);
        imageCell{3,4}   = rand(100);
        titleCell{3,4}  = 'title';
        imageCell{4,1}   = rand(100);
        imageCell{4,2}   = rand(100);
        imageCell{4,3}   = rand(100);
        imageCell{4,4}   = rand(100);
        titleCell{4,4}  = 'title';

        
        fid         = newFigure;
        fid.Name    = ['example ',mfilename];   
 		showMultipleImages(imageCell, 'fid', fid, 'titles', titleCell);
                
    end
end
    