
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function quickExportData(data, filename, exportFolder, clim, cmap)

    if nargin < 2
        filename = [];
    end
    if nargin < 3
        exportFolder = [];
    end
    if nargin < 4
        clim = [];
    end
    if nargin < 5
        cmap = [];
    end
    
    if isempty(exportFolder) 
        if exist([erase(mfilename('fullpath'),'quickExportData'), '.quickExport.config'],'file')
            load([erase(mfilename('fullpath'),'quickExportData'), '.quickExport.config'], '-mat', 'config');
        else 
            config.path = '.';
        end
        exportFolder = config.path;
    end
    
    if isOctave
        if strcmp(exportFolder(end), '/')
            exportFolder = exportFolder(1:end-1);
        end
    else
        exportFolder = strip(exportFolder, 'right', '/');
    end
    fid = figure('Visible', false); clf;
    useFigure(fid);
    subFigure(111,'Preset', 'minimal');
    if isempty(clim)
        imagesc(data);
    else
        imagesc(data, clim);
    end
    axis image;
    axis off;
    resizeFigure(fid, size(data));        
    if isempty(filename)
        filename = ['export',datestr(now,'yymmddHHMMss')];
    end
    exportFigure([exportFolder, '/', filename, '.pdf'], fid, 'pdf', 'colormap', cmap, 'forceVector', true);
    disp(['exported file: ', exportFolder, '/  ', filename, '  .pdf']);
    exportFigure([exportFolder, '/', filename, '.png'], fid, 'png', 'colormap', cmap);
    disp(['exported file: ', exportFolder, '/  ', filename, '  .png']);
    
    close(fid);
    pause(1);

end