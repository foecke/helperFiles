
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function exportColorbar(clim, cmap, filename, path, yScale, colorScale)

    if nargin < 1 || isempty(clim)
        clim = [0,1];
    end
    if nargin < 2
        cmap = [];
    end
    if nargin < 3
        filename = [];
    end
    if nargin < 4
        path = [];
    end
    if nargin < 5
        yScale = 1;
    end
    if nargin < 6
        colorScale = 'linear';
    end
    
    
    fid = figure('Visible', false); clf;
    if isempty(filename)
        filename = ['colorbar_',datestr(now,'yymmddHHMMss')];
    end

%%
    c = colorbar;
    if ~isempty(cmap)
        colormap(cmap);
    end
    axis off;
    ax = fid.CurrentAxes;

    cmin = clim(1);
    cmax = clim(2);

    caxis([cmin, cmax]);
    ax.Units = 'normalized';
%     fid.Position(3) = 150;
%     fid.Position(4) = 400;
% 
%     ax.Position = [-1.8 0.0025 1.8 1-0.005];
    fid.Position(4) = fid.Position(4) * yScale;
    
    set(gca,'ColorScale',colorScale);
    c.Ruler.Exponent = 0;
    cTicksStore = c.TickLabels;
    switch colorScale
        case 'linear'
            c.Ticks = [c.Ticks(1)+((cmax-cmin)*0.0125/yScale) ,c.Ticks(2:end-1), c.Ticks(end)-(cmax-cmin)*0.0125/yScale];
        case 'log'
            c.Ticks = [10^(log10(c.Ticks(1))+(log10(cmax)-log10(cmin))*0.025/yScale) ,c.Ticks(2:end-1), 10^(log10(c.Ticks(end))-(log10(cmax)-log10(cmin))*0.025/yScale)];
    end
    c.TickLabels = cTicksStore; 
%%
    quickExportFigure(fid, filename, path);
    close(fid);    
end