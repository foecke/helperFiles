
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function quickExportSetDefaultPath(path)

    config.path = path;

    save([erase(mfilename('fullpath'),'quickExportSetDefaultPath'), '.quickExport.config'], 'config');

end