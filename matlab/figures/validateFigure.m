%VALIDATEFIGURE checks if a figure ID is valid (and not deleted).
%
%   fidOut = VALIDATEFIGURE(fidIn) checks if fidIn is a valid. If so it
%   returns fidIn as fidOut, otherwise it generates a new figure.
%
%   See also RFIGURE, ISFIGURE.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       06-Feb-2018

%   https://gitlab.com/foecke/helperFiles

function fidOut = validateFigure(fidIn)

    if isFigure(fidIn)
        fidOut = fidIn;    
    else
        warning('off','backtrace');
        warning('invalid figure -> created new figure handle');
        warning('on','backtrace');
        fidOut = newFigure;
    end
end