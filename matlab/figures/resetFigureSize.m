%RESETFIGURESIZE resets the given figures Position

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       18-Jan-2021

%   https://gitlab.com/foecke/helperFiles

function fid = resetFigureSize(fid)

    curPos = fid.Position;
    defaultPos = get(0,'defaultfigureposition');
    fid.Position = [curPos(1:2),defaultPos(3:4)];

end

