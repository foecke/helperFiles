
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function quickExportFigure(fid, filename, exportFolder, mode)

    if nargin < 2
        filename = [];
    end
    if nargin < 3
        exportFolder = [];
    end
    if nargin < 4
        mode = 'both';
    end
    
    if isempty(exportFolder) 
        if exist([erase(mfilename('fullpath'),'quickExportFigure'), '.quickExport.config'],'file')
            load([erase(mfilename('fullpath'),'quickExportFigure'), '.quickExport.config'], '-mat', 'config');
        else 
            config.path = '.';
        end
        exportFolder = config.path;
    end
    
    if isOctave
        if strcmp(exportFolder(end), '/')
            exportFolder = exportFolder(1:end-1);
        end
    else
        exportFolder = strip(exportFolder, 'right', '/');
    end
    
    useFigure(fid);
    if isempty(filename)
        filename = ['export',datestr(now,'yymmddHHMMss')];
    end
    switch mode
        case 'pdf'
            exportFigure([exportFolder, '/', filename, '.pdf'], fid, 'pdf');
            disp(['exported file: ', exportFolder, '/  ', filename, '  .pdf']);
        case 'pdfVector'
            exportFigure([exportFolder, '/', filename, '.pdf'], fid, 'pdf', 'forceVector', true);
            disp(['exported file: ', exportFolder, '/  ', filename, '  .pdf']);
        case 'png'
            exportFigure([exportFolder, '/', filename, '.png'], fid, 'png');
            disp(['exported file: ', exportFolder, '/  ', filename, '  .png']);
        otherwise
            exportFigure([exportFolder, '/', filename, '.pdf'], fid, 'pdf', 'forceVector', true);
            disp(['exported file: ', exportFolder, '/  ', filename, '  .pdf']);
            exportFigure([exportFolder, '/', filename, '.png'], fid, 'png');
            disp(['exported file: ', exportFolder, '/  ', filename, '  .png']);
    end
    pause(1);

end