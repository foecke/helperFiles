%NEWFIGURE opens a figure depending on headless mode of matlab
%   NEWFIGURE behaves exactly like figure, however if MATLAB is running in
%   headless mode (i.e. on a remote server) then the figure is opened non
%   visibly. In this case MATLAB does not attempt to open a window for the
%   generated figure.
%
%   See also FIGURE, ISHEADLESS.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       14-Sep-2017

%   https://gitlab.com/foecke/helperFiles
%%

function fid = newFigure(varargin)
    
	if isHeadless
		fid = figure(varargin{:},'Visible', 'off');
	else
		fid = figure(varargin{:});
	end
	
    if isOctave()
        set(fid, 'PaperPositionMode', 'auto');
    else
        fid.PaperPositionMode = 'auto';
    end
	
    % bx = copyobj(ax, fid) % copy axis ax into other figure fid and call
    % it bx
    
end
