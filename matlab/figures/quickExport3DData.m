
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function quickExport3DData(data, filename, exportFolder, clim, cmap)
      
    if nargin < 2
        filename = [];
    end
    if nargin < 3
        exportFolder = [];
    end
    if nargin < 4
        clim = [];
    end
    if nargin < 5
        cmap = [];
    end
    
    if isempty(filename)
        filename = ['export',datestr(now,'yymmddHHMMss')];
    end
    if isempty(clim)
        clim = [min(data(:)), max(data(:))];
    end
    for i = 1:size(data,3)        
        quickExportData(data(:,:,i), sprintf(['%s_slice%0',num2str(floor(log10(size(data,3)))+1),'i'], filename, i), exportFolder, clim, cmap)        
    end

end