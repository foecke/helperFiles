%EXPORTFIGURE exports a figure into pdf as is. The output papersize is
%adjusted accordingly.
%   
%   EXPORTFIGURE(filename, fid) exports the figure fid to <filename>.pdf
%   EXPORTFIGURE(filename, fid, type) exports the figure fid to 
%       <filename>.<type>, where type may be one of the following
%       filetypes: pdf (default), png, jpg, svg.
%
%   

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       11-Oct-2018

%   https://gitlab.com/foecke/helperFiles

function exportFigure(filename, varargin)
        
    %% parse inputs
    p = inputParser;    
    p.addRequired('filename');
    p.addOptional('fid', gcf, @(x) isFigure(x));
    p.addOptional('type', 'pdf', @(s) any(strcmp({'pdf', 'PDF', '-dpdf', 'PNG', 'png', '-dpng', 'JPG', 'jpg', 'JPEG', 'jpeg', '-djpeg', 'SVG', 'svg', '-dsvg'}, s)));
    p.addParameter('forceVector', false, @islogical);
    p.addParameter('colormap', []);
    
    p.parse(filename, varargin{:});
    
    filename	= p.Results.filename;
    fid         = p.Results.fid;
    type        = p.Results.type;
    forceVector = p.Results.forceVector;
    cmap        = p.Results.colormap;
    
    
    %% function
    
    curU    = get(fid, 'Units');
    set(fid, 'Units', 'centimeters');
    curPPM  = get(fid, 'PaperPositionMode');
    set(fid, 'PaperPositionMode', 'Auto');
    curPU   = get(fid, 'PaperUnits');
    set(fid, 'PaperUnits', 'centimeters');
    pos     = get(fid, 'Position');
    curPS   = get(fid, 'PaperSize');
    set(fid, 'PaperSize', [pos(3), pos(4)]);
    
    
    switch type
        case {'PNG', 'png', '-dpng'}
            outType = '-dpng';
        case {'JPG', 'jpg', 'JPEG', 'jpeg', '-djpeg'}
            outType = '-djpeg';
        case {'SVG', 'svg', '-dsvg'}
            outType = '-dsvg';
        otherwise
            outType = '-dpdf';
    end
    
    curRenderer = get(fid, 'Renderer');
    if forceVector
        set(fid, 'Renderer', 'painters');
    end    
    
    warning('off', 'MATLAB:print:FigureTooLargeForPage');
    if ~isempty(cmap)
        colormap(cmap);
    end
    print(fid,filename,outType); 
    
    set(fid, 'Renderer', curRenderer);
    set(fid, 'Units', curU);
    set(fid, 'PaperPositionMode', curPPM);
    set(fid, 'PaperUnits', curPU);
    set(fid, 'PaperSize', curPS);
     
    
end
