%USEFIGURE sets given function as current figure

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       30-Apr-2018

%   https://gitlab.com/foecke/helperFiles

function fid = useFigure(fid)

    if isFigure(fid)
        set(0, 'CurrentFigure', fid);
    elseif nargout > 0
        fid = validateFigure(fid);
    else 
        error('invalid figure handle');
    end

end
