%ISFIGURE returns true if function handle is a figure, false otherwise.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       11-Oct-2018

%   https://gitlab.com/foecke/helperFiles

function bool = isFigure(fid)
    if isOctave()
        bool = ~isempty(fid) && ishandle(fid);
    else
        bool = ~isempty(fid) && ishandle(fid) && isvalid(fid);
    end
end