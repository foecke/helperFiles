%RESIZEFIGURE resizes a given figure without moving it out of window
%   bounds. This means, the top left corner of the figure window stays
%   where it was.
%
%   RESIZEFIGURE(fidIn): "GUESSING MODE"
%               if no second argument is available, RESIZEFIGURE tries to
%               make a suitable guess based on your monitor dimensions. 
%               Note: This does not work in HEADLESS mode. When using
%               RESIZEFIGURE with SUBPLOT or SUBFIGURE it uses the
%               figure properties of the last used/made plot.
%   RESIZEFIGURE(fidIn, scale) 
%               if second argument is scalar, the given figure is scaled
%               accordingly by the given factor
%   RESIZEFIGURE(fidIn, [sizeX, sizeY])
%               if second argument is vector values, the given figure
%               dimension is set to its respective size in pixel. 
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       12-Jun-2019

%   https://gitlab.com/foecke/helperFiles

function resizeFigure(varargin)
    
    %% parse inputs
    
    gr = groot; 
    sc = get(gr,'ScreenSize');

    p = inputParser;    
    p.addOptional('fidIn', gcf, @(x) any(isFigure(x)));
    p.addOptional('dim', [], @(x) any(numel(x) == [0,1,2]));
    p.addParameter('maxWidth', 0.4*sc(3), @isnumeric);
    p.addParameter('maxHeight', 0.5*sc(4), @isnumeric);
    p.parse(varargin{:});
    
    fid         = p.Results.fidIn;
    dim         = p.Results.dim;
    maxWidth	= p.Results.maxWidth;
    maxHeight	= p.Results.maxHeight;
    
    useFigure(fid);drawnow();
    
    %% function
    
    if isOctave()
        fidUnitsOld = get(fid, 'Units');
        set(fid, 'Units', 'pixels');
        tmpPos = get(fid, 'Position');
        auxw = tmpPos(2) + tmpPos(4);
    else        
        fidUnitsOld = fid.Units;
        fid.Units = 'pixels';
        auxw = fid.Position(2) + fid.Position(4);
    end
    
    if numel(dim) == 0
        if isHeadless
            warning('SKIPPING: ''Guessing mode'' for resizeFigure is not available in headless mode! Please set a rescale factor or the desired image size.');
        else
            % make a proper guess
            if isOctave()
                allAxes = findobj(get(fid,'Children'),'Type','axes');
            else
                allAxes = findobj(fid.Children,'Type','axes');
            end
            
            
            if isempty(allAxes)                
                error('no content, hence no ''Guessing mode'' available');
        
                
            elseif numel(allAxes)==1 %single image figure
                if isOctave()                    
                    axpbar = get(get(fid, 'CurrentAxes'), 'PlotBoxAspectRatio');
                else
                    axpbar = fid.CurrentAxes.PlotBoxAspectRatio;
                end                
                rat = axpbar(1)/axpbar(2);
                   
            else  %subplot/subFigure figure                
                nGridx = zeros(numel(allAxes), 1);
                nGridy = nGridx;
                for i = 1:numel(allAxes)
                    if isOctave()
                        tmpPos = get(allAxes(i), 'Position');
                        nGridx(i) = tmpPos(1);
                        nGridy(i) = tmpPos(2);
                    else
                        nGridx(i) = allAxes(i).Position(1);
                        nGridy(i) = allAxes(i).Position(2);
                    end
                end
                if isOctave()
                    rat = numel(unique(nGridx))/numel(unique(nGridy));
                else
                    rat = numel(uniquetol(nGridx,1e-2))/numel(uniquetol(nGridy,1e-2));
                end
                
            end
            
            if rat < 1 % portrait mode
                newh = maxHeight;
                neww = min(newh*rat,maxWidth);
                newh = neww/rat;
            else % landscape mode
                neww = maxWidth;
                newh = min(neww/rat,maxHeight);
                neww = newh*rat;
            end  
            if isOctave()
                tmpPos = get(fid, 'Position');
                set(fid, 'Position', [tmpPos([1,2]),ceil(neww),ceil(newh)])
            else
                fid.Position([3,4]) = [ceil(neww),ceil(newh)];
            end
        end
    elseif numel(dim) == 1
        % rescale figure
        if isOctave()
            tmpPos = get(fid, 'Position');
            set(fid, 'Position', [tmpPos([1,2]),dim.*tmpPos([3,4])])
        else
            fid.Position([3,4]) = dim.*fid.Position([3,4]);
        end
    else
        % resize to given size
        if isOctave()
            tmpPos = get(fid, 'Position');
            set(fid, 'Position', [tmpPos([1,2]),dim(2),dim(1)])
        else
            fid.Position([3,4]) = [dim(2),dim(1)];
        end
    end  
    
    if isOctave()
        tmpPos = get(fid, 'Position');
        set(fid, 'Position', [tmpPos(1),auxw - tmpPos(4),tmpPos([3,4])])
        set(fid, 'Units', fidUnitsOld);
    else
        fid.Position(2) = auxw - fid.Position(4);
        fid.Units = fidUnitsOld;
    end
    

end
