%SUBFIGURE creates axes in tiled positions
%   h = SUBFIGURE(nmp [, settings])
%   h = SUBFIGURE(n, m, p [, settings])
%   h = SUBFIGURE(n, m, x, y [, settings])
%   h = SUBFIGURE(n, m, x, y, w, h [, settings])
%   separates the figure window into a n-by-m grid. It returns an axes
%   handle h, which is optional to save. The position in the grid is
%   given by the p-th tile or (x,y) position in the grid. For axis spanning
%   over mutliple tile use w and h. 
%   SUBFIGURE also suppports short notation: SUBFIGURE(nmp [, settings]) is
%   equivalent to SUBFIGURE(n, m, p [, settings]).
%
%   h = SUBFIGURE(..., settings) allows optional parameter:
%       'holdaxis'      hold on             (Default: 0)
%       'sh'            spacing horizontal  (Default: 0)
%       'sv'            spacing vertical    (Default: 0)
%       'mt'            margin top          (Default: 0)
%       'mb'            margin bottom       (Default: 0)
%       'ml'            margin left         (Default: 0)
%       'mr'            margin right        (Default: 0)
%       'pt'            padding top         (Default: 0.01)
%       'pb'            padding bottom      (Default: 0.01)
%       'pl'            padding left        (Default: 0.01)
%       'pr'            padding right       (Default: 0.01)
%       'preset'        choose one of the following presets: no presets
%                       implented yet. If a preset is given the other 
%                       setting are ignored.
%                       Available presets:
%                       'default' as above
%                       'subplot' as of subplot
%                       'minimal' no borders
%                       'axis'    allows some space for axis
%       'cla'           clear current axis  (Default: false)
%   Note that a given preset overide all optional spacing parameters. Also
%   given presets are only tested on small number of images, therefore
%   individual tweaking might be necessary.
%
%   Example: (run SUBFIGURE() without any argument to show example)
%
%       fid = rfigure;
%       fid.Name = ['example ',mfilename];
%         
%       settings = {'sh', 0.01, 'sv', 0.01};
%         
%       subFigure(331,'preset','minimal'); imagesc(rand(4));   axis off;
%       subFigure(3,3,2,settings{:});      imagesc(rand(8));   axis off;
%       subFigure(3,3,1,2,settings{:});    imagesc(rand(16));  axis off;
%       subFigure(3,3,5:6,settings{:});    imagesc(rand(4,8)); axis off;
%       subFigure(3,3,1,3,3,1,settings{:});imagesc(rand(4,12));axis off;
%
%   See also SUBPLOT.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       05-Feb-2018

%   https://gitlab.com/foecke/helperFiles

%   This code is loosely based on the subaxis code by 
%   Aslak Grinsted 2001-2014 (available on Mathworks:
%   https://de.mathworks.com/matlabcentral/fileexchange/3696-subaxis-subplot)
%%


function ax = subFigure(varargin)

    %% run example?
    if nargin == 0
        runExample;
        return;
    end
    
    %% parsing inputs
   
    settings = {};
    
    hasSettings = any(~cellfun(@isnumeric, varargin));
    if hasSettings 
        posSettings = find(~cellfun(@isnumeric, varargin),1);
        settings = varargin(posSettings:end);
        varargin(posSettings:end) = [];        
    end
    
    p   = inputParser;
    p.addParameter('holdaxis', false, @islogical);
    p.addParameter('sh', 0, @isnumeric);
    p.addParameter('sv', 0, @isnumeric);
    p.addParameter('mt', 0, @isnumeric);
    p.addParameter('mb', 0, @isnumeric);
    p.addParameter('ml', 0, @isnumeric);
    p.addParameter('mr', 0, @isnumeric);
    p.addParameter('pt', 0.01, @isnumeric);
    p.addParameter('pb', 0.01, @isnumeric);
    p.addParameter('pl', 0.01, @isnumeric);
    p.addParameter('pr', 0.01, @isnumeric);
    p.addParameter('preset', 'none', @(s) any(strcmp({'pixel','subplot','minimal','axis','title'}, s)));
    p.addParameter('cla', false, @islogical);
    p.parse(settings{:});
    
    holdaxis = p.Results.holdaxis;
    sh  = p.Results.sh;
    sv  = p.Results.sv;
    mt  = p.Results.mt;
    mb  = p.Results.mb;
    ml  = p.Results.ml;
    mr  = p.Results.mr;
    pt  = p.Results.pt;
    pb  = p.Results.pb;
    pl  = p.Results.pl;
    pr  = p.Results.pr;
    preset = p.Results.preset;
    doCla = p.Results.cla;
        
    %% parse coordinates    
    if numel(varargin)==1&&ceil(log10(varargin{1}))==3
        aux = num2str(varargin{1});
        varargin{1} = str2double(aux(1));
        varargin{2} = str2double(aux(2));
        varargin{3} = str2double(aux(3));        
    end
    
    rows = varargin{1};
    cols = varargin{2};
    
    switch numel(varargin)
        case 3
            if numel(varargin{3})==2
                [x1, y1] = ind2sub([cols rows],varargin{3}(1));
                [x2, y2] = ind2sub([cols rows],varargin{3}(2));
            else 
                x1  = mod((varargin{3}-1),cols)+1;
                x2  = x1;
                y1  = floor((varargin{3}-1)/cols)+1;
                y2  = y1;
            end
        case 4
                x1  = varargin{3};
                x2  = x1;
                y1  = varargin{4};
                y2  = y1;
        case 6
                x1  = varargin{3};
                x2  = x1+varargin{5}-1;
                y1  = varargin{4};
                y2  = y1+varargin{6}-1;
        otherwise
            error('parsing error');
    end
    
    %% parse preset    
    switch preset
        case 'pixel'
            s = 2;              %spacing between images in pixel	
            x = 500;            %figure width
            xi = x/cols;        %size of a single subplot
            y = rows * (xi+2*s); %figure height
            sRelx = s/x;
            sRely = s/y;

            sh = sRelx;sv = sRely;  
            mt = 0;mb = 0;ml = 0;mr = 0;
            pt = 0;pb = 0;pl = 0;pr = 0;   
            
        case 'axis'
            s = 25;              %spacing between images in pixel	
            x = 500;            %figure width
            xi = x/cols;        %size of a single subplot
            y = rows * (xi+2*s); %figure height
            sRelx = s/x;
            sRely = s/y;

            sh = 0;sv = 0;  
            mt = 0;mb = 0;ml = 0;mr = 0;
            pt = sRely;pb = 2*sRely;pl = 1.5*sRelx;pr = 0.5*sRelx;   
            
        case 'subplot'            
            sh = 0;sv = 0;
            mt = 0;mb = 0;ml = 0;mr = 0;
            pt = 0.075;
            pb = 0.075;
            pl = 0.075;
            pr = 0.075;
 
        case 'minimal'            
            sh = 0;sv = 0;
            mt = 0;mb = 0;ml = 0;mr = 0;
            pt = 0;pb = 0;pl = 0;pr = 0;  
            
        case 'title'            
            sh = 0;sv = 0;
            mt = 0;mb = 0;ml = 0;mr = 0;
            pt = 0.05;pb = 0;pl = pt/2;pr = pt/2;  
    end
        
        
    %% calculate position

    cellwidth   = ((1-ml-mr) - (cols-1)*sh) / cols;
    cellheight  = ((1-mt-mb) - (rows-1)*sv) / rows;
    xpos1       = ml + pl + cellwidth  * (x1-1) + sh * (x1-1);
    xpos2       = ml - pr + cellwidth  *  x2    + sh * (x2-1);
    ypos1       = mt + pt + cellheight * (y1-1) + sv * (y1-1);
    ypos2       = mt - pb + cellheight *  y2    + sv * (y2-1);

    %% define axes
    if holdaxis
        if nargout==0
            axes('position',[xpos1, 1-ypos2, xpos2-xpos1, ypos2-ypos1]);
        else
            ax	= axes('position',[xpos1, 1-ypos2, xpos2-xpos1, ypos2-ypos1]);
        end
    else
        if nargout==0
            subplot('position',[xpos1, 1-ypos2, xpos2-xpos1, ypos2-ypos1]);
        else
            ax  = subplot('position',[xpos1, 1-ypos2, xpos2-xpos1, ypos2-ypos1]);
        end
    end
    if exist('ax', 'var')
        if isOctave()
            set(ax, 'Box', 'on');
        else
            ax.Box = 'on';
        end
    end    
    
    if doCla
        cla;
    end
    
    %% example
    function runExample()

        fid = newFigure;
        fid.Name = ['example ',mfilename];
        
        fid.Position(3:4) = [400,400];
        
        settings = {'sh', 0.01, 'sv', 0.01, 'preset', 'pixel'};
        
        subFigure(331);         imagesc(rand(4));   axis image;
        subFigure(3,3,2,'preset','axis');imagesc(rand(8));   axis image;
        subFigure(3,3,1,2,3,1,settings{:}); imagesc(rand(4,12));axis image;axis off;
        subFigure(3,3,1,3,settings{:});     imagesc(rand(16));  axis image;axis off;
        subFigure(3,3,8:9,'pt',0.05);       imagesc(rand(4,8)); axis image;
        title('made some space for a title');   
        
    end
end


