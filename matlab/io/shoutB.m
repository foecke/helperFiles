%SHOUTB prints formatted text to console
%   SHOUTB(s) prints string s to console and appends a newline "\n"
%
%   SHOUTB works best in combination with SHOUT.
%
%   Example: (run SHOUTB without any argument to show example)
%
%   See also SHOUT, SHOUTA, SHOUTAB.
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       06-Feb-2018

%   https://gitlab.com/foecke/helperFiles
%%

function shoutB(s)
	
    %% run example?
    if nargin == 0
        runExample;
        return;
    end
    
    %% code
	fprintf('%s\n',s);

    %% example	
    function runExample()
        
        shoutA('create rand matrix of size:');
        n = 5;
        shoutB([num2str(n), 'x', num2str(n)]);
		m = rand(n);        
		shoutAB('# of entries > 0.5:',num2str(sum(m(:)>0.5)));   

    end
end
