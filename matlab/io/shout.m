%SHOUT(s) prints text to console
%
%   See also FOUTA, FOUTB, FOUTAB.
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       06-Feb-2018

%   https://gitlab.com/foecke/helperFiles
%%

function shout(s)

	fprintf('%s', s);

end

