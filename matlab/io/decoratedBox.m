
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function decoratedBox(text, boxSymbol)

    if nargin < 2
        boxSymbol = '#';
    end

    fprintf('\n%s\n%s %s %s\n%s\n\n', ...
        repmat(boxSymbol,1,length(text)+6), ...
        repmat(boxSymbol,1,2), ...
        text, ...
        repmat(boxSymbol,1,2), ...
        repmat(boxSymbol,1,length(text)+6));

end