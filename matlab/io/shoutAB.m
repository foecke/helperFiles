%SHOUTAB prints formatted text to console
%   
%   SHOUTAB(s1, s2 [, spacing]) combines SHOUT(s1 [, spacing]) and SHOUTB(s2).
%   SHOUTAB(s1, s2, spacing) is spacing a single numerical value it is simply
%   forwarded to SHOUT(s1, spacing).
%   SHOUTAB(s1, s2, [spacing1, spacing2]) is spacing an array of two elements
%   then these elements are interpreted as relative proportion of the full
%   with of the command window. Hence [50,50] splits the command window
%   into two parts (same with [1,1], [1337,1337], et cetera.). Hence [0,1]
%   pushes the second input to the very left side; [1,2] makes the first
%   part using one third of the screen; [1,0] pushes the second input to
%   the right edge.
%
%   Example: (run SHOUTAB without any argument to show an example)
%
%   See also SHOUT, SHOUTA, SHOUTB.
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       15-Nov-2018

%   https://gitlab.com/foecke/helperFiles
%%

function shoutAB(s1, s2, varargin)

    %% run example?
    if nargin == 0
        runExample;
        return;
    end
    
    %% parse inputs
	
    p = inputParser;    
    p.addOptional('spacing',50,@(s) isnumeric(s) && (numel(s)==1 || numel(s)==2));
    p.parse(varargin{:});
    
    fSpacing	= p.Results.spacing;
    
    %% parse spacing
    if numel(fSpacing)==1
        fSpace = fSpacing;
    else
        cws = get(0, 'CommandWindowSize');
        rOffset = cws(1) - length(s2) - 1;
        fSpace = max(0, min(floor(cws(1).*fSpacing(1)./sum(fSpacing)),rOffset));
    end
    
    %% code
    shoutA(s1, fSpace);
    shoutB(s2);

    %% example	
    function runExample()
        
		shoutB('shoutAB EXAMPLES:'); 
        shoutB('');
		shoutB('EXPLICIT SPACING:'); 
		shoutAB('This is the default spacing:','shoutAB( )');   
		shoutAB('Hence we can also set explicitly:','shoutAB( ,50)', 50); 
		shoutAB('or:','shoutAB( ,10)', 10); 
		shoutAB('there is at least one space:','shoutAB( ,10)', 10); 
        shoutB('');
		shoutB('RELATIVE SPACING:');   
		shoutAB('Split 0% - 100%:','shoutAB( ,[0,1])', [0,1]);   
		shoutAB('Split 1/4 - 3/4:','shoutAB( ,[1,3])', [1,3]);  
		shoutAB('Split 1/3 - 2/3:','shoutAB( ,[1,2])', [1,2]);  
		shoutAB('Split 1/2 - 1/2:','shoutAB( ,[1,1])', [1,1]);     
		shoutAB('Split 2/3 - 1/3:','shoutAB( ,[2,1])', [2,1]);   
		shoutAB('Split 3/4 - 1/4:','shoutAB( ,[3,1])', [3,1]);     
		shoutAB('Split 100% - 0%:','shoutAB( ,[1,0])', [1,0]);  

    end
end
    