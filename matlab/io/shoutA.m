%SHOUTA prints formatted text to console
%   SHOUT(s [, spacing]) prints string s to console and appends whitespace
%   so that the total number of characters is spacing = 50 (default). The 
%   used spacing can be changed using optional parameter spacing.
%
%   SHOUTA works best in combination with SHOUTB.
%
%   Example: (run SHOUTA without any argument to show example)
%
%   See also SHOUT, SHOUTB, SHOUTAB.
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       06-Feb-2018

%   https://gitlab.com/foecke/helperFiles
%%

function shoutA(s, varargin)

    %% run example?
    if nargin == 0
        runExample;
        return;
    end
    
    %% parse inputs
	
    p = inputParser;    
    p.addOptional('spacing',50,@(s) isnumeric(s) && numel(s)==1);
    p.parse(varargin{:});
    
    fSpace	= max((length(s) + 1), p.Results.spacing);
    
    %% code
	fprintf(['%-',num2str(fSpace),'s'],s);
  
    %% example	
    function runExample()
        
        shoutA('create rand matrix of size:');
        n = 5;
        shoutB([num2str(n), 'x', num2str(n)]);
		m = rand(n);        
		shoutAB('# of entries > 0.5:',num2str(sum(m(:)>0.5)));   

    end
end
