

%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-05-2022

%   https://gitlab.com/foecke/helperFiles


function runAndReturn(fileName)

    curWD = pwd;
    run(fileName);
    cd(curWD);

end