%ISHEADLESS determine if current MATLAB session is headless.
%   ISHEADLESS() returns 1 (true) if the current matlab session is in
%   headless mode (i.e. on a remote machine) and 0 (false) otherwise.
%
%   See also NEWFIGURE.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       13-Oct-2017

%   https://gitlab.com/foecke/helperFiles
%%

function p = isHeadless()
    if isOctave()
        p = false; % assume octave is started with GUI
    else
        p = strcmp(java.lang.System.getProperty('java.awt.headless'),'true');
    end
end
