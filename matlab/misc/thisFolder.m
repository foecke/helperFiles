%THISFOLDER returns the full folder path of the script, that this command
%is used in.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       13-Jun-2019

%   https://gitlab.com/foecke/helperFiles

function out = thisFolder(varargin)

    if nargin==0
        folder = '';
    else
        folder = ['/',varargin{1}];
    end
    
    %% function
    callStack = dbstack(1, '-completenames');
    if isempty(callStack)
        out = [pwd,folder];
    else
        out = [fileparts(callStack(1).file),folder];
    end
    
end
