
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function [activationMeasurementMap, activeActivations, activeMeasurements] = selectorMaps(activations, measurements, selectorOptions)

    numMeas               	= numel(measurements);
    numActi               	= numel(activations);
    
    measurementMap          = zeros(numMeas,numActi);
    
    if isfield(selectorOptions, 'rngSeed')
        rng(selectorOptions.rngSeed);
    end
    
    switch selectorOptions.measurementType
        case 'all'
            measurementMap = ones(numMeas,numActi);
        case 'list'  
            measurementMap = repmat(sparse(ismember(measurements,selectorOptions.measurementParameter))', [1, numActi]);
        case 'randomFraction'      
            qp  = min(ceil(selectorOptions.measurementParameter*numMeas),numMeas); %effective rays by factor raysF   
            for j=1:numActi
                measurementMap(:,j) = randperm(numMeas)'<=qp;        
            end
        case 'randomCount'
            measurementMap = repmat(randperm(numMeas)'<=selectorOptions.measurementParameter, [1, numActi]);
        case 'randomCountEach'
            for j=1:numActi
                measurementMap(:,j) = randperm(numMeas)'<=selectorOptions.measurementParameter;
            end
        otherwise
            error('unknown measurement type');
    end

    
    switch selectorOptions.activationType
        case 'all'
            activeActivations = ones(1,numActi);
        case 'list'
            activeActivations = sparse(ismember(activations,selectorOptions.activationParameter));
        case 'randomFraction'
            pp  = min(ceil(selectorOptions.activationParameter*numActi),numActi); %effective rays by factor raysF   
            activeActivations = sparse(randperm(numActi)<=pp);
        case 'randomCount'
            activeActivations = sparse(randperm(numActi)<=selectorOptions.activationParameter);
        otherwise
            error('unknown activation type');
    end    
    
    
    activationFullMap           = sparse(repmat(activeActivations, [size(measurementMap,1),1]));
    activationMeasurementMap    = sparse(activationFullMap & logical(measurementMap));
    activeMeasurements          = measurementMap(:,activeActivations);
    

end

