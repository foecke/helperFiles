%VARNAME returns the workspace variable name as string
%   Let testName = 1337; then s = VARNAME(testName) returns a String Array 
%   s associated with the String 'testName'.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       05-Feb-2018

%   https://gitlab.com/foecke/helperFiles
%%

function out = varname(varargin)
	out = inputname(1);
end