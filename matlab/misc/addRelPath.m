%ADDRELPATH adds the given path relative to the current path
%
%   Use the '--gen' flag to use genpath
%   Note: ADDRELPATH can not be used with F9, since F9 does not provide
%   any information of what script the snipped is executed from.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       05-Jun-2019

%   https://gitlab.com/foecke/helperFiles

function addRelPath(folder, varargin)

    %% parsing    
    p = inputParser;
    
    p.addRequired('folder');
    p.addOptional('tag', '' ,@(s) any(strcmp({'','--gen'}, s)));
       
    p.parse(folder, varargin{:});
    
    folder           = p.Results.folder;
    tag           = p.Results.tag;    
    
    callStack = dbstack(1, '-completenames');
    
    dirPath = '.';
    if (size(callStack,1)) > 0
        dirPath = fileparts(callStack(1).file);
    end
    if strcmp({'--gen'}, tag)
        addpath(genpath([dirPath,'/',folder]));
    else
        addpath([dirPath,'/',folder]);
    end
    
end
