
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg

%   https://gitlab.com/foecke/helperFiles

function out = isOctave()
    out = exist('OCTAVE_VERSION', 'builtin') ~= 0;
end    