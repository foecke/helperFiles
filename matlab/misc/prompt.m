%PROMPT creates a prompt with a default value and dependency check.
%
%   run:
%       PROMPT
%
%   to see a minimal example.

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       24-Jun-2019

%   https://gitlab.com/foecke/helperFiles

function out = prompt(promptText, defaultAnswer, answerType)
    
    %% run example?
    if nargin == 0
        out = runExample;
        return;
    end
    if nargin < 2
        defaultAnswer = [];
    end
    if nargin < 3
        answerType = class(defaultAnswer);
    end
        
    %% function
    switch answerType
        case 'double'
            functionHandle = @(s) ~isinf(str2double(s)) & ~isnan(str2double(s)) & isnumeric(str2double(s));
            defaultAnswerText = num2str(defaultAnswer);
            parseFunction = @str2double;
        case 'string'
            functionHandle = @isstring;
            defaultAnswerText = defaultAnswer;
            parseFunction = @(s) ['',s];
        case 'char'
            functionHandle = @ischar;
            defaultAnswerText = defaultAnswer;
            parseFunction = @(s) ['',s];
        case 'logical'
            functionHandle = @(s) any(strcmp({'true','false'},s));
            if defaultAnswer 
                defaultAnswerText = 'true';
            else
                defaultAnswerText = 'false';
            end
            parseFunction = @(s) ~strcmp(s, 'false');
        otherwise
            defaultAnswerText = '[]';
    end
    
    answered = false;
    
    while ~answered
        out = input([promptText, ' [default: ',defaultAnswerText,'] >> '],'s');
        if isempty(out)
            out = defaultAnswer;
        end
        if functionHandle(out)
            answered = true;
            out = parseFunction(out);
        else
            shoutB('The answer is not valid!');
        end
    end
    
    %% example
    function out = runExample()
        
        out = prompt('Is this an Example prompt?', true);
                
    end
end
