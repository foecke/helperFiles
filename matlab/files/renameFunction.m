%RENAMEFUNCTION renames a function and creates a redirect
%
%   out = RENAMEFUNCTION(oldPathToFile, newPathToFile) moves function
%   oldPathToFile to newPathToFile and creates a redirect. Therefore old
%   code still works, however a warning is thrown that the old file may be
%   removed in the future.
%
%   run:
%       RENAMEFUNCTION();
%
%   to see a minimal example, that creates a new function 'testfileOld' and
%   renames it to 'testfileNew'; 

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       14-Mar-2018

%   https://gitlab.com/foecke/helperFiles

function out = renameFunction(oldPathToFile, newPathToFile)
    
    %% run example?
    if nargin == 0
        out = runExample;
        return;
    end
    
    %% parse inputs    
    if ~strcmp(oldPathToFile(end-1:end),'.m')
        oldPathToFile = [oldPathToFile,'.m'];
    end  
    oldPathSplit    = strsplit(oldPathToFile,'/');
    oldFileName     = oldPathSplit{end}(1:end-2); %remove .m at the end
    
    if ~strcmp(newPathToFile(end-1:end),'.m')
        newPathToFile = [newPathToFile,'.m'];
    end 
    newPathSplit    = strsplit(newPathToFile,'/');
    newPath         = strjoin(newPathSplit(1:end-1),'/');    
    newFileName     = newPathSplit{end}(1:end-2); %remove .m at the end
    
    %% catch all exceptions cases   
    
    %source file does not exist
    if exist(oldPathToFile,'file') == 0 
        out = 'The file to rename does not exist. Aborting.';
        return;         
    %target folder does not exist    
    elseif exist(newPath,'dir') == 0 && ~isempty(newPath) 
        prompt = sprintf('\nThe output folder does not exist:\ncreate folder ''%s''? (y/n): ',newPath);
        ansPrompt = input(prompt,'s');
        if strcmp(ansPrompt,'y') || strcmp(ansPrompt,'yes')
            mkdir(newPath);
        else            
            out = 'The output folder does not exist. Aborting.';
            return;            
        end        
    %target file already exist
    elseif exist(newPathToFile,'file') == 2 
        prompt = sprintf('\nThere is already a file called ''%s'':\noverride ''%s''? (y/n): ',newPathToFile,newPathToFile);
        ansPrompt = input(prompt,'s');
        if strcmp(ansPrompt,'y') || strcmp(ansPrompt,'yes')
            % delete(newFilename);
        else  
            out = 'There is already file with that name. Aborting.';
            return;      
        end
    end 
    
    %% copy file
    copyfile(oldPathToFile,newPathToFile); 
    
    %% rename variables in target file
    str = fileread(newPathToFile);
    str = strrep(str, oldFileName, newFileName);
    str = strrep(str, upper(oldFileName),upper(newFileName));
    
    fid = fopen(newPathToFile, 'w');
    fwrite(fid,str,'*char');
    fclose(fid);
    
    %% replace source file 
    
    str = '';
    str = [str, '%''',oldFileName,''' redirects all arguments to ''',newFileName,'''', newline];
    str = [str, '', newline];
    str = [str, 'function out = ',oldFileName,'(varargin)', newline];
    str = [str, '', newline];
    str = [str, '	addpath(''', newPath,''');', newline];
    str = [str, '', newline];
    str = [str, '	callStack = dbstack(1, ''-completenames'');', newline];
    str = [str, '	callWarning = '''';', newline];
    str = [str, '	if ~isempty(callStack)', newline];
    str = [str, '		callFile = callStack(1).file;', newline];
    str = [str, '		callLine = callStack(1).line;', newline];
    str = [str, '		callWarning = sprintf(''\nYou used ''''addCoilSequence.m'''' in %s (line %i).\n\n'', callFile, callLine);', newline];
    str = [str, '	end', newline];
    str = [str, '', newline];
    str = [str, '	warn = sprintf(''IMPORTANT:\n\nThe function ''''',oldPathToFile,''''' has moved to ''''',newPathToFile,'''''.\nAs of now a redirect keeps existing code valid.\n%sPlease update your code as soon as possible!'', callWarning);', newline];
    str = [str, '', newline];
    str = [str, '	if nargin == 0', newline];
    str = [str, '		out = ',newFileName,';', newline];
    str = [str, '	else', newline];
    str = [str, '		out = ',newFileName,'(varargin{:});', newline];
    str = [str, '	end', newline];
    str = [str, '	warning(''prog:renamed'',warn);', newline];
    str = [str, '	', newline];
    str = [str, 'end', newline];
    fid = fopen(oldPathToFile, 'w');
    fwrite(fid,str,'*char');
    fclose(fid);
    
    %% everything worked!    
    out = 'success';
    
    %% example
    function out = runExample()
        
        newFunction('testFile', false);
        oldPathToFile = 'testFile';
        newPathToFile = 'testFileNewPath';		
        out = renameFunction(oldPathToFile,newPathToFile);
                
    end
end
