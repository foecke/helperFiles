%NEWFUNCTION creates a new function with given template
%
%   run:
%       NEWFUNCTION('simpleCalc.m');
%
%   or without any arguments to create a simple calculator in
%   file 'simpleCalc.m'. 
%   In the current state, it will create a new function that includes the 
%   basic help scheme as well as an example that will run by pressing F5.
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       13-Sep-2016

%   https://gitlab.com/foecke/helperFiles

function newFunction(filename, openEditor, configfile)

    %% run example?
    if nargin == 0
        runExample;
        return;
    end
        
    %% parse inputs
    if ~strcmp(filename(end-1:end),'.m')
        filename = [filename,'.m'];
    end    
    if nargin < 2
        openEditor = true;
    end 
    if nargin < 3
        configfile = [];
    end
    
    filenamewo = strsplit(filename,'/');
    filenamewo = filenamewo{end};
    filenamewo = filenamewo(1:end-2);
    
    %% override on exist?
    if exist(filename, 'file')        
        prompt = 'File exist! Override? Y/N [N]: ';
        str = input(prompt,'s');
        if isempty(str)
            str = 'N';
        end
        if ~any(strcmp(str,{'Y','y','yes'}))
            return
        end
    end
    
    %% load config file   
    if exist(configfile,'file')
        %configfile
        load(configfile, '-mat', 'config');
    elseif exist([erase(mfilename('fullpath'),'newFunction'), '.newFunction.config'],'file')
        %[mfilename('fullpath'), '.config']
        load([erase(mfilename('fullpath'),'newFunction'), '.newFunction.config'], '-mat', 'config');
    else 
        config = [];
    end
    
    if any(~isfield(config, {'name', 'mail', 'affi'}))
        config.name = [];
        config.mail = [];
        config.affi = [];
        
        disp(['No author information specified. Please update once: ', newline, 'This info will be stored locally in hidden file ''.newFunction.config''.']);
        prompt = 'Author name:        ';
        while isempty(config.name)
            config.name = input(prompt,'s');
        end
        prompt = 'Author E-Mail:      ';
        while isempty(config.mail)
            config.mail = input(prompt,'s');
        end
        prompt = 'Author Affiliation: ';
        while isempty(config.affi)
            config.affi = input(prompt,'s');
        end                
        save([erase(mfilename('fullpath'),'newFunction'), '.newFunction.config'], 'config');
    end
        
    %% parse file content      
    str = '';
    str = [str, '%',upper(filenamewo),' is a function', newline];
    str = [str, '%', newline];
    str = [str, '%   run:', newline];
    str = [str, '%       ',upper(filenamewo), newline];
    str = [str, '%', newline];
    str = [str, '%   to see a minimal example.', newline];
    str = [str, '', newline];
    str = [str, '%   Author:     ',config.name, newline];
    str = [str, '%   E-Mail:     ',config.mail, newline];
    str = [str, '%   Institute:  ',config.affi, newline];
    str = [str, '%   Date:       ',datestr(datetime('today')), newline];
    str = [str, '', newline];
    str = [str, 'function out = ',filenamewo,'(a,varargin)', newline];
    str = [str, '    ', newline];
    str = [str, '    %% run example?', newline];
    str = [str, '    if nargin == 0', newline];
    str = [str, '        out = runExample;', newline];
    str = [str, '        return;', newline];
    str = [str, '    end', newline];
    str = [str, '    ', newline];
    str = [str, '    %% parse inputs', newline];
    str = [str, '    p = inputParser;    ', newline];
    str = [str, '    p.addRequired(''a'');', newline];
    str = [str, '    p.addOptional(''b'', 1, @isnumeric);', newline];
    str = [str, '    p.addParameter(''type'', ''+'', @(x) any(validatestring(x,{''+'',''-'',''*'',''/''})));', newline];
    str = [str, '    ', newline];
    str = [str, '    p.parse(a,varargin{:});', newline];
    str = [str, '    ', newline];
    str = [str, '    a       = p.Results.a;', newline];
    str = [str, '    b       = p.Results.b;', newline];
    str = [str, '    type    = p.Results.type;', newline];
    str = [str, '    ', newline];
    str = [str, '    %% function', newline];
    str = [str, '    switch type', newline];
    str = [str, '        case ''+''   ', newline];
    str = [str, '            out = a + b; ', newline];
    str = [str, '        case ''-''', newline];
    str = [str, '            out = a - b; ', newline];
    str = [str, '        case ''*''', newline];
    str = [str, '            out = a * b; ', newline];
    str = [str, '        case ''/''', newline];
    str = [str, '           if b==0', newline];
    str = [str, '           	error(''divide by zero. aborting'');', newline];
    str = [str, '           else', newline];
    str = [str, '               out = a / b;', newline];
    str = [str, '           end', newline];
    str = [str, '    end', newline];
    str = [str, '    ', newline];
    str = [str, '    %% example', newline];
    str = [str, '    function out = runExample()', newline];
    str = [str, '        ', newline];
    str = [str, '        disp(''Example Code: (5 + 3) / 2 = '');', newline];
    str = [str, '        a = 5;', newline];
    str = [str, '        b = 3;', newline];
    str = [str, '        out = ',filenamewo,'(a,b);                 % 5 + 3 = 8', newline];
    str = [str, '        out = ',filenamewo,'(out,2,''type'',''/'');    % 8 / 3 = 2.6667', newline];
    str = [str, '                ', newline];
    str = [str, '    end', newline];
    str = [str, 'end', newline];
    
    %% write new file    
    fid = fopen(filename, 'w');
    if (fid == -1)
        error('Something is wrong with the specified file path. Maybe the folder does not exist yet?');
    end
    fwrite(fid,str,'*char');
    fclose(fid);
    
    %% open new file
    if openEditor && ~strcmp(java.lang.System.getProperty('java.awt.headless'),'true')
        edit(filename);
    end
    
    %% example
    function runExample()        
        newFunction('simpleCalc')
    end

end
