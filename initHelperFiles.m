%INITHELPERFILES adds toolbox subfolder to matlab path
%
%   run:
%       INITHELPERFILES
%

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       17-Jan-2017

%   https://gitlab.com/foecke/helperFiles

function initHelperFiles()

	rootFolder = fileparts(mfilename('fullpath'));
		
    addpath([rootFolder,'/matlab']);
    addpath([rootFolder,'/matlab/figures']);
    addpath([rootFolder,'/matlab/files']);
    addpath([rootFolder,'/matlab/imaging']);
    addpath([rootFolder,'/matlab/io']);
    addpath([rootFolder,'/matlab/misc']);
	
end
