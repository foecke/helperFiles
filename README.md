Helper Files
============

The helperFiles package gives nice little tools to visualize image and
volume data, as well as tools to create formatted output.

Dependencies:
-------------
* Matlab >= R2017b
* Image Processing Toolbox

How to install:
---------------
This toolbox does not require a specific spot to be stored. Please choose the folder at your pleasure. In the following example we store it under `~/matlab/helperFiles`. For unix systems we use
```bash
cd ~/matlab/
git clone https://gitlab.com/foecke/helperFiles
cd ./helperFiles/
```

Usage:
------
To use the helperFiles toolbox simply use `addpath` with the chosen save spot. In case it is stored under `~/matlab/helperFiles` please run the following lines on matlab once (or on top of your script file):
```matlab
addpath('~/matlab/helperFiles/');
initHelperFiles();
```
Then all functions are available.

Examples:
---------
Most examples are included in the function itself.

Authors:
--------
* Lea Föcke ([lea.foecke@fau.de](mailto:lea.foecke@fau.de))

